let enterdetails=document.getElementById('submit');
enterdetails.addEventListener("click",addDetails);


let row=2;

function addDetails(){
    let vehicle_id=document.getElementById("vehicle-id").value;
    let vehicle_name=document.getElementById("vehicle-name").value;
    let brand_name=document.getElementById("brand").value;
    let regis_num=document.getElementById("reg-no").value;
    let color=document.getElementById("color").value;
    let customer_name=document.getElementById("customer-name").value;
    let deleteBtn = document.createElement('button');
    let updatebtn=document.createElement("button");
    updatebtn.setAttribute("onclick","update()");
    updatebtn.setAttribute("id",row);
    deleteBtn.appendChild(document.createTextNode('Edit'));
    deleteBtn.setAttribute("id",row);
    deleteBtn.setAttribute("value",row);
    deleteBtn.setAttribute("onclick","removeElement(this.id)");
    deleteBtn.appendChild(document.createTextNode('X'));

    if(!vehicle_id || !vehicle_name || !brand_name || !regis_num || !color || !customer_name){
        alert("Enter all details ");
        return;
    }
    let table=document.getElementById("car-details");
    let newRow=table.insertRow(row);
    let c1=newRow.insertCell(0);
    let c2=newRow.insertCell(1);
    let c3=newRow.insertCell(2);
    let c4=newRow.insertCell(3);
    let c5=newRow.insertCell(4);
    let c6=newRow.insertCell(5);
    let c7=newRow.insertCell(6);

    c1.innerHTML=vehicle_id;
    c2.innerHTML=vehicle_name;
    c3.innerHTML=brand_name;
    c4.innerHTML=regis_num;
    c5.innerHTML=color;
    c6.innerHTML=customer_name;
    c7.appendChild(deleteBtn);
    row++;
   
}

let removeElement=function(id){
    document.getElementById("car-details").deleteRow(id);
}

let table=document.getElementById("car-details");

            
for(var i = 1; i < table.rows.length; i++){
    table.rows[i].onclick = function(){
        document.getElementById("vehicle-id").value = this.cells[0].innerHTML;
        document.getElementById("vehicle-name").value = this.cells[1].innerHTML;
        document.getElementById("brand").value = this.cells[2].innerHTML;
        document.getElementById("reg-no").value = this.cells[3].innerHTML;
        document.getElementById("color").value = this.cells[4].innerHTML;
        document.getElementById("customer-name").value = this.cells[5].innerHTML;
    };
}

let update=function(id){
    table.rows[id].cells[0].innerHTML = document.getElementById("vehicle-id").value;
    table.rows[id].cells[1].innerHTML = document.getElementById("vehicle-name").value;
    table.rows[id].cells[2].innerHTML = document.getElementById("brand").value;
    table.rows[id].cells[3].innerHTML = document.getElementById("reg-no").value;
    table.rows[id].cells[4].innerHTML = document.getElementById("color").value;
    table.rows[id].cells[6].innerHTML = document.getElementById("customer-name").value;
}